/*Available Roles =
--Standard Squad--
"I_A3_AAF_SquadLeader", "I_A3_AAF_Default", "I_A3_AAF_Medic", "I_A3_AAF_MG", "I_A3_AAF_Grenadier", "I_A3_AAF_LAT", "I_A3_AAF_Engineer", "I_A3_AAF_Marksman"
--MG Squad--
"I_A3_AAF_TeamLeader_MG", "I_A3_AAF_MG"
--Heavy AT Squad--
"I_A3_AAF_TeamLeader_HAT", "I_A3_AAF_HAT"
--AA Squad--
"I_A3_AAF_TeamLeader_AA", "I_A3_AAF_AA"
--Sniper Squad--
"I_A3_AAF_Spotter", "I_A3_AAF_Sniper"*/

class I_A3_AAF_Squad_Basic1 {
	defaultSquad = 1;
	displayName = "Alpha";
	description = "Infantry";
	side = TEAM_EAST;
	type = 2;
	roles[] = {
		{"I_A3_AAF_SquadLeader"},
		{"I_A3_AAF_Default",1},
		{"I_A3_AAF_Medic",1},
		{"I_A3_AAF_MG", 1},
		{"I_A3_AAF_Default", 4},
		{"I_A3_AAF_Grenadier", 4},
		{"I_A3_AAF_LAT", 4},
		{"I_A3_AAF_Engineer", 6},
		{"I_A3_AAF_Marksman", 8}
	};
};

class I_A3_AAF_Squad_Basic2: I_A3_AAF_Squad_Basic1 {
	defaultSquad = 0;
	displayName = "Bravo";
	description = "Infantry";
};


class I_A3_AAF_Squad_MG1 {
	defaultSquad = 1;
	displayName = "Charlie";
	description = "MG";
	side = TEAM_EAST;
	type = 2;
	availableAt = 15;
	roles[] = {
		{"I_A3_AAF_TeamLeader_MG"},
		{"I_A3_AAF_MG",1},
		{"I_A3_AAF_MG",2}
	};
};

class I_A3_AAF_Squad_Sniper1 {
	defaultSquad = 1;
	displayName = "Noobs";
	description = "Sniper Team";
	side = TEAM_EAST;
	type = 2;
	availableAt = 30;
	roles[] = {
		{"I_A3_AAF_Spotter"},
		{"I_A3_AAF_Sniper",1}
	};
};