/*Available Roles =
--Standard Squad--
"O_A3_CSAT_SquadLeader", "O_A3_CSAT_Default", "O_A3_CSAT_Medic", "O_A3_CSAT_MG", "O_A3_CSAT_Grenadier", "O_A3_CSAT_LAT", "O_A3_CSAT_Engineer", "O_A3_CSAT_Marksman"
--MG Squad--
"O_A3_CSAT_TeamLeader_MG", "O_A3_CSAT_MG"
--Heavy AT Squad--
"O_A3_CSAT_TeamLeader_HAT", "O_A3_CSAT_HAT"
--AA Squad--
"O_A3_CSAT_TeamLeader_AA", "O_A3_CSAT_AA"
--Sniper Squad--
"O_A3_CSAT_Spotter", "O_A3_CSAT_Sniper"
--Recon (SpecOps) Squad--
"O_A3_CSAT_TeamLeader_Recon", "O_A3_CSAT_Rifleman_Recon", "O_A3_CSAT_Medic_Recon", "O_A3_CSAT_Engineer_Recon", "O_A3_CSAT_LAT_Recon" */

class O_A3_CSAT_Squad_Basic1 {
	defaultSquad = 1;
	displayName = "Alpha";
	description = "Infantry";
	side = TEAM_EAST;
	type = 2;
	roles[] = {
		{"O_A3_CSAT_SquadLeader"},
		{"O_A3_CSAT_Default",1},
		{"O_A3_CSAT_Medic",1},
		{"O_A3_CSAT_MG", 1},
		{"O_A3_CSAT_Default", 4},
		{"O_A3_CSAT_Grenadier", 4},
		{"O_A3_CSAT_LAT", 4},
		{"O_A3_CSAT_Engineer", 6},
		{"O_A3_CSAT_Marksman", 8}
	};
};

class O_A3_CSAT_Squad_Basic2: O_A3_CSAT_Squad_Basic1 {
	defaultSquad = 0;
	displayName = "Bravo";
	description = "Infantry";
};


class O_A3_CSAT_Squad_MG1 {
	defaultSquad = 1;
	displayName = "Charlie";
	description = "MG";
	side = TEAM_EAST;
	type = 2;
	availableAt = 15;
	roles[] = {
		{"O_A3_CSAT_TeamLeader_MG"},
		{"O_A3_CSAT_MG",1},
		{"O_A3_CSAT_MG",2}
	};
};

class O_A3_CSAT_Squad_Recon1 {
	defaultSquad = 1;
	displayName = "Delta";
	description = "Recon";
	side = TEAM_EAST;
	type = 2;
	availableAt = 20;
	roles[] = {
		{"O_A3_CSAT_TeamLeader_Recon"},
		{"O_A3_CSAT_Rifleman_Recon",1},
		{"O_A3_CSAT_Medic_Recon",1},
		{"O_A3_CSAT_Rifleman_Recon", 3},
		{"O_A3_CSAT_Engineer_Recon", 3},
		{"O_A3_CSAT_LAT_Recon", 5},
	};
};

class O_A3_CSAT_Squad_Sniper1 {
	defaultSquad = 1;
	displayName = "Noobs";
	description = "Sniper Team";
	side = TEAM_EAST;
	type = 2;
	availableAt = 30;
	roles[] = {
		{"O_A3_CSAT_Spotter"},
		{"O_A3_CSAT_Sniper",1}
	};
};

