#define COMMON_ITEMS {"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}
#define COMMON_MEDICAL {"FRL_fieldDressing", 3}, {"FRL_Morphine", 1}, {"FRL_epinephrine", 2}


class G_A3_FIA_Default {
	displayName = "str_a3_cfgvehicles_b_soldier_f0";
	abilities[] = {};
	icon = "\z\frl\addons\kits\ui\icons\rifleman_small_88.paa";
	marker = "IconMan";

	class Clothing {
		uniform   = "U_BG_Guerilla2_3";
		headgear  = "H_Shemag_olive";
		goggles   = "";
		vest      = "V_Chestrig_oli";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_TRG21_F";
				rail        = "acc_flashlight";
				optics      = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}};
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"Binocular", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};

		class Variant2 : Variant1 {
			displayName = "AR Support";

			class Backpack {
				backpack    = "B_FieldPack_oli";
				content[] = {{"200Rnd_65x39_cased_Box", 3}};
			};
		};

		class Variant3 : Variant2 {
			displayName = "AT Support";

			class Backpack : Backpack {
				content[] = {{"RPG32_F", 2}};
			};
		};
	};
};

class G_A3_FIA_SquadLeader : G_A3_FIA_Default {
	displayName = "str_b_soldier_sl_f0";
	abilities[] = {"RP"};
	icon = "\z\frl\addons\kits\ui\icons\sqleader_small_88.paa";
	marker = "IconManOfficer";

	class Clothing : Clothing {
		uniform   = "U_BG_Guerrilla_6_1";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}, {"Rangefinder", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};
	};
};

class G_A3_FIA_TeamLeader_MG : G_A3_FIA_SquadLeader {
	displayName = "str_b_soldier_tl_f0";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AR Support";

			class Backpack {
				backpack    = "B_FieldPack_oli";
				content[] = {{"200Rnd_65x39_cased_Box", 3}};
			};
		};
	};
};

class G_A3_FIA_TeamLeader_AA : G_A3_FIA_TeamLeader_MG {

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AA Support";

			class Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class G_A3_FIA_Medic : G_A3_FIA_Default {
	displayName = "str_b_medic_f0";
	abilities[] = {"Medic"};
	icon = "\z\frl\addons\kits\ui\icons\medic_small_88.paa";
	marker = "IconManMedic";

	class Clothing : Clothing {
		uniform   = "U_BG_Guerilla2_1";
	};

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack{
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_Morphine", 5}, {"FRL_epinephrine", 10}, {"SmokeShell", 4}};
			};
		};
	};
};

class G_A3_FIA_MG : G_A3_FIA_Default {
	displayName = "str_b_soldier_ar_f0";
	icon = "\z\frl\addons\kits\ui\icons\autorifleman_small_88.paa";
	marker = "IconManMG";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "LMG_Mk200_F";
				bipod       = "bipod_03_F_blk";
				magazines[] = {{"200Rnd_65x39_cased_Box", 5}};
			};
		};
	};
};

class G_A3_FIA_Grenadier : G_A3_FIA_Default {
	displayName = "str_b_soldier_gl_f0";
	icon = "\z\frl\addons\kits\ui\icons\grenadier_small_88.paa";
	marker = "IconManExplosive";

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_TRG21_GL_F";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class G_A3_FIA_LAT : G_A3_FIA_Default {
	displayName = "str_b_soldier_lat_f0";
	icon = "\z\frl\addons\kits\ui\icons\LAT.paa";
	marker = "IconManAT";

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG32_F", 1}};
			};
		};
	};
};

class G_A3_FIA_AA : G_A3_FIA_Default {
	displayName = "str_b_soldier_aa_f0";
	icon = "\z\frl\addons\kits\ui\icons\AA.paa";
	marker = "IconManAT";

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "launch_B_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class G_A3_FIA_Marksman : G_A3_FIA_Default {
	displayName = "str_b_soldier_m_f0";
	icon = "\z\frl\addons\kits\ui\icons\Marksman.paa";
	marker = "IconManRecon";

	class Clothing {
		uniform   = "U_BG_Guerilla3_1";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "srifle_EBR_F";
				optics      = "optic_DMS";
				bipod       = "bipod_03_F_blk";
				magazines[] = {{"20Rnd_762x51_Mag", 9}};
			};
		};
	};
};

class G_A3_FIA_Engineer : G_A3_FIA_Default {
	displayName = "str_b_engineer_f0";
	abilities[] = {"Engineer"};
	icon = "\z\frl\addons\kits\ui\icons\engineer_small_88.paa";
	marker = "IconManEngineer";

	class Variants : Variants {
		class Variant1 : Variant2 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"IEDUrbanSmall_Remote_Mag", 2}, {"IEDUrbanBig_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant2 : Variant3 {
			displayName = "AP Minelayer";

			class Backpack : Backpack {
				content[]   = {{"APERSTripMine_Wire_Mag", 1}, {"IEDUrbanSmall_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant3 : Variant3 {
			displayName = "AT Minelayer";

			class Backpack : Backpack {
				content[]   = {{"ATMine_Range_Mag", 1}, {"IEDUrbanSmall_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};
	};
};