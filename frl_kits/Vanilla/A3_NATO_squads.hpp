/*Available Roles =
--Standard Squad--
"B_A3_NATO_SquadLeader", "B_A3_NATO_Default", "B_A3_NATO_Medic", "B_A3_NATO_MG", "B_A3_NATO_Grenadier", "B_A3_NATO_LAT", "B_A3_NATO_Engineer", "B_A3_NATO_Marksman"
--MG Squad--
"B_A3_NATO_TeamLeader_MG", "B_A3_NATO_MG"
--Heavy AT Squad--
"B_A3_NATO_TeamLeader_HAT", "B_A3_NATO_HAT"
--AA Squad--
"B_A3_NATO_TeamLeader_AA", "B_A3_NATO_AA"
--Sniper Squad--
"B_A3_NATO_Spotter", "B_A3_NATO_Sniper"
--Recon (SpecOps) Squad--
"B_A3_NATO_TeamLeader_Recon", "B_A3_NATO_Rifleman_Recon", "B_A3_NATO_Medic_Recon", "B_A3_NATO_Engineer_Recon", "B_A3_NATO_LAT_Recon" */

class B_A3_NATO_Squad_Basic1 {
	defaultSquad = 1;
	displayName = "Alpha";
	description = "Infantry";
	side = TEAM_WEST;
	type = 2;
	roles[] = {
		{"B_A3_NATO_SquadLeader"},
		{"B_A3_NATO_Default",1},
		{"B_A3_NATO_Medic",1},
		{"B_A3_NATO_MG", 1},
		{"B_A3_NATO_Default", 4},
		{"B_A3_NATO_Grenadier", 4},
		{"B_A3_NATO_LAT", 4},
		{"B_A3_NATO_Engineer", 6},
		{"B_A3_NATO_Marksman", 8}
	};
};

class B_A3_NATO_Squad_Basic2: B_A3_NATO_Squad_Basic1 {
	defaultSquad = 0;
	displayName = "Bravo";
	description = "Infantry";
};


class B_A3_NATO_Squad_MG1 {
	defaultSquad = 1;
	displayName = "Charlie";
	description = "MG";
	side = TEAM_WEST;
	type = 2;
	availableAt = 15;
	roles[] = {
		{"B_A3_NATO_TeamLeader_MG"},
		{"B_A3_NATO_MG",1},
		{"B_A3_NATO_MG",2}
	};
};

class B_A3_NATO_Squad_Recon1 {
	defaultSquad = 1;
	displayName = "Delta";
	description = "Recon";
	side = TEAM_WEST;
	type = 2;
	availableAt = 20;
	roles[] = {
		{"B_A3_NATO_TeamLeader_Recon"},
		{"B_A3_NATO_Rifleman_Recon",1},
		{"B_A3_NATO_Medic_Recon",1},
		{"B_A3_NATO_Rifleman_Recon", 3},
		{"B_A3_NATO_Engineer_Recon", 3},
		{"B_A3_NATO_LAT_Recon", 5},
	};
};

class B_A3_NATO_Squad_Sniper1 {
	defaultSquad = 1;
	displayName = "Noobs";
	description = "Sniper Team";
	side = TEAM_WEST;
	type = 2;
	availableAt = 30;
	roles[] = {
		{"B_A3_NATO_Spotter"},
		{"B_A3_NATO_Sniper",1}
	};
};

