#define COMMON_ITEMS {"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}
#define COMMON_MEDICAL {"FRL_fieldDressing", 3}, {"FRL_Morphine", 1}, {"FRL_epinephrine", 2}


class I_A3_AAF_Default {
	displayName = "str_a3_cfgvehicles_b_soldier_f0";
	abilities[] = {};
	icon = "\z\frl\addons\kits\ui\icons\rifleman_small_88.paa";
	marker = "IconMan";

	class Clothing {
		uniform   = "U_I_CombatUniform";
		headgear  = "H_HelmetIA";
		goggles   = "";
		vest      = "V_PlateCarrierIA1_dgtl";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_Mk20_F";
				rail        = "acc_flashlight";
				optics      = "optic_ACO_grn";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}};
			};

			class Pistol {
				weapon      = "hgun_P07_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"Binocular", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};

		class Variant2 : Variant1 {
			displayName = "CQB";

			class Primary {
				weapon      = "hgun_PDW2000_F";
				rail        = "";
				optics      = "optic_ACO_grn_smg";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_9x21_Mag", 9}};
			};
		};

		class Variant3 : Variant1 {
			displayName = "AR Support";

			class Backpack {
				backpack    = "B_AssaultPack_mcamo";
				content[] = {{"200Rnd_65x39_cased_Box", 3}};
			};
		};

		class Variant4 : Variant3 {
			displayName = "AT Support";

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 2}};
			};
		};
	};
};

class I_A3_AAF_SquadLeader : I_A3_AAF_Default {
	displayName = "str_b_soldier_sl_f0";
	abilities[] = {"RP"};
	icon = "\z\frl\addons\kits\ui\icons\sqleader_small_88.paa";
	marker = "IconManOfficer";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}, {"Rangefinder", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};
	};
};

class I_A3_AAF_TeamLeader_MG : I_A3_AAF_SquadLeader {
	displayName = "str_b_soldier_tl_f0";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AR Support";

			class Backpack {
				backpack    = "B_AssaultPack_mcamo";
				content[] = {{"200Rnd_65x39_cased_Box", 3}};
			};
		};
	};
};

class I_A3_AAF_TeamLeader_AA : I_A3_AAF_TeamLeader_MG {

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AA Support";

			class Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class I_A3_AAF_TeamLeader_HAT : I_A3_AAF_TeamLeader_MG {

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HAT Support";

			class Backpack {
				content[] = {{"Titan_AP", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class I_A3_AAF_Spotter : I_A3_AAF_SquadLeader {
	displayName = "str_b_spotter_f0";

	class Clothing {
		uniform   = "U_I_GhillieSuit";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_oli";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class I_A3_AAF_Medic : I_A3_AAF_Default {
	displayName = "str_b_medic_f0";
	abilities[] = {"Medic"};
	icon = "\z\frl\addons\kits\ui\icons\medic_small_88.paa";
	marker = "IconManMedic";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack{
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_Morphine", 5}, {"FRL_epinephrine", 10}, {"SmokeShell", 4}};
			};
		};
	};
};

class I_A3_AAF_MG : I_A3_AAF_Default {
	displayName = "str_b_soldier_ar_f0";
	icon = "\z\frl\addons\kits\ui\icons\autorifleman_small_88.paa";
	marker = "IconManMG";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "LMG_Mk200_F";
				bipod       = "bipod_03_F_blk";
				magazines[] = {{"200Rnd_65x39_cased_Box", 5}};
			};
		};
	};
};

class I_A3_AAF_Grenadier : I_A3_AAF_Default {
	displayName = "str_b_soldier_gl_f0";
	icon = "\z\frl\addons\kits\ui\icons\grenadier_small_88.paa";
	marker = "IconManExplosive";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_Mk20_GL_F";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class I_A3_AAF_LAT : I_A3_AAF_Default {
	displayName = "str_b_soldier_lat_f0";
	icon = "\z\frl\addons\kits\ui\icons\LAT.paa";
	marker = "IconManAT";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};

class I_A3_AAF_HAT : I_A3_AAF_Default {
	displayName = "str_b_soldier_at_f0";
	icon = "\z\frl\addons\kits\ui\icons\HAT.paa";
	marker = "IconManAT";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "launch_B_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}};
			};
		};
	};
};

class I_A3_AAF_AA : I_A3_AAF_Default {
	displayName = "str_b_soldier_aa_f0";
	icon = "\z\frl\addons\kits\ui\icons\AA.paa";
	marker = "IconManAT";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "launch_B_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class I_A3_AAF_Marksman : I_A3_AAF_Default {
	displayName = "str_b_soldier_m_f0";
	icon = "\z\frl\addons\kits\ui\icons\Marksman.paa";
	marker = "IconManRecon";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "srifle_EBR_F";
				optics      = "optic_DMS";
				bipod       = "bipod_03_F_blk";
				magazines[] = {{"20Rnd_762x51_Mag", 9}};
			};
		};
	};
};

class I_A3_AAF_Engineer : I_A3_AAF_Default {
	displayName = "str_b_engineer_f0";
	abilities[] = {"Engineer"};
	icon = "\z\frl\addons\kits\ui\icons\engineer_small_88.paa";
	marker = "IconManEngineer";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"SatchelCharge_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant2 : Variant3 {
			displayName = "AP Minelayer";

			class Backpack : Backpack {
				content[]   = {{"APERSMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant3 : Variant3 {
			displayName = "AT Minelayer";

			class Backpack : Backpack {
				content[]   = {{"ATMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};
	};
};

class I_A3_AAF_Sniper : I_A3_AAF_Spotter {
	displayName = "str_b_sniper_f0";
	abilities[] = {};
	icon = "\z\frl\addons\kits\ui\icons\sniper_small_88.paa";
	marker = "IconManRecon";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_GM6_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS";
				bipod       = "";
				magazines[] = {{"5Rnd_127x108_Mag", 10}};
			};
		};
	};
};

