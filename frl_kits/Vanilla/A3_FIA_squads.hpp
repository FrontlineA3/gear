/*Available Roles =
--Standard Squad--
"G_A3_FIA_SquadLeader", "G_A3_FIA_Default", "G_A3_FIA_Medic", "G_A3_FIA_MG", "G_A3_FIA_Grenadier", "G_A3_FIA_LAT", "G_A3_FIA_Engineer", "G_A3_FIA_Marksman"
--MG Squad--
"G_A3_FIA_TeamLeader_MG", "G_A3_FIA_MG"
--AA Squad--
"G_A3_FIA_TeamLeader_AA", "G_A3_FIA_AA"*/

class G_A3_FIA_Squad_Basic1 {
	defaultSquad = 1;
	displayName = "Alpha";
	description = "Infantry";
	side = TEAM_WEST;
	type = 2;
	roles[] = {
		{"G_A3_FIA_SquadLeader"},
		{"G_A3_FIA_Default",1},
		{"G_A3_FIA_Medic",1},
		{"G_A3_FIA_MG", 1},
		{"G_A3_FIA_Default", 4},
		{"G_A3_FIA_Grenadier", 4},
		{"G_A3_FIA_LAT", 4},
		{"G_A3_FIA_Engineer", 6},
		{"G_A3_FIA_Marksman", 8}
	};
};

class G_A3_FIA_Squad_Basic2: G_A3_FIA_Squad_Basic1 {
	defaultSquad = 0;
	displayName = "Bravo";
	description = "Infantry";
};


class G_A3_FIA_Squad_MG1 {
	defaultSquad = 1;
	displayName = "Charlie";
	description = "MG";
	side = TEAM_WEST;
	type = 2;
	availableAt = 15;
	roles[] = {
		{"G_A3_FIA_TeamLeader_MG"},
		{"G_A3_FIA_MG",1},
		{"G_A3_FIA_MG",2}
	};
};