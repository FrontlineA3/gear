#define TEAM_EAST       0
#define TEAM_WEST       1
#define TEAM_IND		2
#define TEAM_CIV   		3

class FRL_squads {
	//#include "IronFront\IF_Wehrmacht_squads.hpp"
	//#include "IronFront\IF_Soviets_squads.hpp"
	#include "Vanilla\A3_NATO_squads.hpp"
	#include "Vanilla\A3_CSAT_squads.hpp"
	//#include "Vanilla\A3_AAF_squads.hpp"
	//#include "Vanilla\A3_FIA_squads.hpp"
};

class FRL_kits {
	#include "IronFront\IF_Wehrmacht.hpp"
	#include "IronFront\IF_Sturmtroopers.hpp"
	#include "IronFront\IF_Soviets.hpp"
	#include "Vanilla\A3_NATO.hpp"
	#include "Vanilla\A3_CSAT.hpp"
	#include "Vanilla\A3_AAF.hpp"
	#include "Vanilla\A3_FIA.hpp"
};
