#define COMMON_ITEMS {"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}
#define COMMON_MEDICAL {"FRL_fieldDressing", 3}, {"FRL_Morphine", 1}, {"FRL_epinephrine", 2}

class R_IFA_SV_Default {
	type = 1;
	displayName    = "Rifleman";
	icon    = "frl_kits\ui\icons\rifleman_small_88.paa";
	marker  = "IconMan";

	class Clothing
	{
		uniform  = "U_O_CombatUniform_ocamo";
		headgear = "H_HelmetO_ocamo";
		vest     = "V_HarnessO_brn";
		goggles	 = "";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Pistol {};
			class Primary {
				weapon      = "arifle_Katiba_F";
				optics      = "optic_ACO_grn";
				rail        = "";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_green_mag_Tracer", 9}};
			};

			class Secondary {};
			class Backpack {
				backpack    = "B_FieldPack_cbr_LAT";
				content[] = {{"30Rnd_65x39_caseless_green_mag_Tracer", 12}};
			};

			items[] = {{"HandGrenade", 2}, {"SmokeShell", 2}};
			itemshidden[]      = {COMMON_ITEMS, COMMON_MEDICAL};
		};

		class Variant2 : Variant1 {
			displayName = "AR Support";

			class Backpack {
				backpack    = "B_FieldPack_ocamo_AAR";
				content[] = {{"200Rnd_65x39_cased_Box_Tracer", 5}};
			};
		};
	};
};

class R_IFA_SV_SL: R_IFA_SV_Default {
	displayName = "Squad Leader";
	abilities[] = {"RP"};
	class Clothing : Clothing {
		uniform  = "U_O_OfficerUniform_ocamo";
		headgear = "H_Beret_ocamo";
		vest     = "V_BandollierB_khk";
	};
};
class R_IFA_SV_Rifleman: R_IFA_SV_Default {
	displayName = "Rifleman";
};

class R_IFA_SV_Medic: R_IFA_SV_Rifleman {
	displayName = "Medic";
	abilities[] = {"Medic"};
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary
			{
				weapon      = "arifle_MXC_F";
			};
			class Secondary
			{
			};
			class Backpack
			{
				backpack  = "B_AssaultPack_mcamo";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_Morphine", 5}, {"FRL_epinephrine", 10}};
			};

			items[] = {{"HandGrenade", 2}, {"SmokeShell", 5}};
			itemshidden[]      = {COMMON_ITEMS};
		};
	};
};
class R_IFA_SV_MG: R_IFA_SV_Rifleman {
	displayName = "Machinegunner";
	picture = "frl_kits\ui\icons\autorifleman_88.paa";
	icon = "frl_kits\ui\icons\autorifleman_small_88.paa";

	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary
			{
				weapon      = "LMG_Mk200_F";
				magazines[] = {{"200Rnd_65x39_cased_Box_Tracer", 3}};
			};
			class Secondary
			{
			};
			class Backpack
			{
			};
		};
	};

};
class R_IFA_SV_Grenadier: R_IFA_SV_Rifleman {
	displayName = "Grenadier";
};