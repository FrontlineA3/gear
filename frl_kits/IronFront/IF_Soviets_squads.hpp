	class R_IFA_SV_Default {
		defaultSquad = 1;
		displayName = "Squad Anna";
		side = TEAM_RED;
		type = 2;
		roles[] = {
			{"R_IFA_SV_SL"},
			{"R_IFA_SV_Rifleman"},
			{"R_IFA_SV_MG", 1},
			{"R_IFA_SV_Medic", 1},
			{"R_IFA_SV_Grenadier", 2}
		};
	};