#define COMMON_ITEMS {"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}
#define COMMON_MEDICAL {"FRL_fieldDressing", 3}, {"FRL_Morphine", 1}, {"FRL_epinephrine", 2}

class B_IFA_Wehr_Default {
	displayName    = "Rifleman";
	abilities[] = {};
	icon = "frl_kits\ui\icons\sqleader_small_88.paa";
	marker = "IconMan";
	class Clothing {
		uniform   = "U_LIB_GER_Schutze";
		headgear  = "H_LIB_GER_Helmet";
		goggles   = "";
		vest      = "V_LIB_GER_VestKar98";
	};

	class Variants {
		class Variant1 {
			displayName = "Reqular";
			class Primary {
				weapon      = "LIB_K98";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"lib_5Rnd_792x57", 10}};
			};

			class Secondary {
			};

			class Pistol {
			};

			class Backback {
				backpack    = "B_LIB_GER_A_frame";
				content[]   = {};
			};

			items[]       = {{"lib_shg24", 2}, {"FirstAidKit", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};
		class Variant2: Variant1 {};
	};
};

class B_IFA_Wehr_SL: B_IFA_Wehr_Default {
	displayName = "Squad Leader";
	abilities[] = {"RP"};
	icon    = "frl_kits\ui\icons\sqleader_small_88.paa";
	marker = "IconManOfficer";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Regular";
			class Primary : Primary
			{
				weapon      = "arifle_MX_F";
				muzzle      = "muzzle_snds_H";
				rail        = "";
				optics      = "optic_Hamr";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_mag", 2}, {"30Rnd_65x39_caseless_mag_Tracer", 3}};
			};
			class Backpack {};
			class Pistol {
				weapon      = "hgun_Pistol_heavy_01_F";
				muzzle      = "muzzle_snds_acp";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"11Rnd_45ACP_Mag", 3}};
			};

			items[] = {{"itemGPS", 1}, {"HandGrenade", 2}, {"SmokeShell", 2}};
		};
	};
};

class B_IFA_Wehr_Rifleman: B_IFA_Wehr_Default {
	displayName = "Rifleman";
};

class B_IFA_Wehr_MG: B_IFA_Wehr_Rifleman {
	displayName = "Machinegunner";
	icon = "frl_kits\ui\icons\autorifleman_small_88.paa";
	marker = "IconManMG";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Light Support";
			class Primary : Primary {
				weapon      = "arifle_MX_SW_F";
				muzzle      = "muzzle_snds_H";
				rail        = "acc_pointer_IR";
				optics      = "optic_tws_mg";
				bipod       = "bipod_01_F_snd";
				magazines[] = {{"100Rnd_65x39_caseless_mag_Tracer", 6}};
			};
			class Backpack {};
		};
		class Variant2 : Variant2 {
			displayName = "MMG";
			class Primary : Primary {
				weapon      = "MMG_02_camo_F";
				muzzle      = "muzzle_snds_338_sand";
				rail        = "";
				optics      = "optic_Holosight";
				bipod       = "bipod_01_F_mtp";
				magazines[] = {{"130Rnd_338_Mag", 2}};
			};
			class Backpack {};
		};
	};
};
class B_IFA_Wehr_Medic: B_IFA_Wehr_Rifleman {
	displayName = "Medic";
	abilities[] = {"Medic"};
	icon    = "frl_kits\ui\icons\medic_small_88.paa";
	marker = "IconManMedic";
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary {
				weapon      = "arifle_MXC_F";
			};
			class Secondary {};
			class Backpack {
				backpack  = "B_AssaultPack_mcamo";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_Morphine", 5}, {"FRL_epinephrine", 10}};
			};

			items[] = {{"HandGrenade", 2}, {"SmokeShell", 5}};
			itemshidden[] = {COMMON_ITEMS};
		};
	};
};

class B_IFA_Wehr_Engineer: B_IFA_Wehr_Rifleman {
	displayName = "Engineer";
	abilities[] = {"Engineer"};
	icon    = "frl_kits\ui\icons\engineer_small_88.paa";
	marker = "IconManEngineer";
};

class B_IFA_Wehr_Grenadier: B_IFA_Wehr_Rifleman {
	displayName = "Grenadier";
	icon = "frl_kits\ui\icons\grenadier_small_88.paa";
	marker = "IconManExplosive";
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary {
				weapon      = "arifle_MX_GL_F";
				magazines[] = {{"30Rnd_65x39_caseless_mag_Tracer", 9}, {"1Rnd_HE_Grenade_shell", 8}, {"1Rnd_Smoke_Grenade_shell", 4}};
			};
			class Secondary {};
			class Backpack {};
			items[] = {};
		};
	};
};