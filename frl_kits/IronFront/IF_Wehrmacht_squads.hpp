	class B_IFA_Wehr_Squad1 {
		defaultSquad = 1;
		displayName = "Squad Alpha";
		side = TEAM_BLU;
		type = 2;
		roles[] = {
			{"B_IFA_Wehr_SL"},
			{"B_IFA_Wehr_Rifleman",1},
			{"B_IFA_Wehr_Medic",2},
			{"B_IFA_Wehr_MG", 3},
			{"B_IFA_Wehr_Engineer", 3},
			{"B_IFA_Wehr_Rifleman", 4},
			{"B_IFA_Wehr_Grenadier", 5},
			{"B_IFA_Wehr_Medic", 6}
		};
	};