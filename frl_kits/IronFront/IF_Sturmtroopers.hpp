class B_IFA_Sturm_SL: B_IFA_Wehr_SL {
	class Clothing {
		uniform  = "U_B_CombatUniform_mcam";
		headgear = "H_HelmetB";
		goggles  = "";
		vest     = "V_PlateCarrier2_rgr";
	};
};

class B_IFA_Sturm_Rifleman: B_IFA_Wehr_Rifleman {

	class Clothing {
		uniform  = "U_B_CombatUniform_mcam";
		headgear = "H_HelmetB";
		goggles  = "";
		vest     = "V_PlateCarrier2_rgr";
	};
};

class B_IFA_Sturm_MG: B_IFA_Wehr_MG {
	class Clothing {
		uniform  = "U_B_CombatUniform_mcam";
		headgear = "H_HelmetB";
		goggles  = "";
		vest     = "V_PlateCarrier2_rgr";
	};
};
class B_IFA_Sturm_Medic: B_IFA_Wehr_Medic {
	class Clothing {
		uniform  = "U_B_CombatUniform_mcam";
		headgear = "H_HelmetB";
		goggles  = "";
		vest     = "V_PlateCarrier2_rgr";
	};
};

class B_IFA_Sturm_Engineer: B_IFA_Wehr_Engineer {
	class Clothing {
		uniform  = "U_B_CombatUniform_mcam";
		headgear = "H_HelmetB";
		goggles  = "";
		vest     = "V_PlateCarrier2_rgr";
	};
};

class B_IFA_Sturm_Grenadier: B_IFA_Wehr_Grenadier {
	class Clothing {
		uniform  = "U_B_CombatUniform_mcam";
		headgear = "H_HelmetB";
		goggles  = "";
		vest     = "V_PlateCarrier2_rgr";
	};
};