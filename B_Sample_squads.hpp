	class B_CUP_US_Squad1 { // -- Classname can be whatever you want.
		defaultSquad = 1; // -- 1 if this default squad type. 0 if not. If it's default squad type, it's the squad type that gets used when you create a new squad (Only  put 1 to default)
		displayName = "Squad Alpha"; // -- Name shown in squad list
		side = TEAM_BLU; // -- TEAM_BLU or TEAM_RED
		type = 2; // -- Put on 2 for now
		roles[] = { // -- Array with the roles in this squad
			{"B_CUP_US_SL"}, // Array in format: [Classname of the role you want, players needed to use said role (No number means 0 for first role)]
			{"B_CUP_US_Rifleman", 1},
			{"B_CUP_US_Medic", 1},
			{"B_CUP_US_MG", 2},
			{"B_CUP_US_Engineer", 3},
			{"B_CUP_US_Grenadier", 4},
			{"B_CUP_US_Rifleman", 5},
			{"B_CUP_US_Medic", 6}
		};
	};

	class B_CUP_US_Specops {
		displayName = "SpecOps";
		side = TEAM_BLU;
		type = 2;
		availableAt = 20; // -- Number of players required for this squad to open up (Not implemented yet, but will be used in future)
		roles[] = {
			{"B_CUP_US_Specops_SL"},
			{"B_CUP_US_Specops_Marksman", 1},
			{"B_CUP_US_Specops_Specialist", 1}
		};
	};

	class B_CUP_US_SniperElement: B_CUP_US_SpecOps {
		displayName = "Recon Team";
		availableAt = 20;
		isElement = 1; // -- Can attach itself to normal infantry squads (Joins their group (Used their comms, waypoints and RP)). Not implemented yet
		roles[] = {
			{"B_CUP_US_Specops_SL"},
			{"B_CUP_US_Specops_Marksman", 1}
		};
	};
