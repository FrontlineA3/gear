#define COMMON_ITEMS {"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}
#define COMMON_MEDICAL {"FRL_fieldDressing", 3}, {"FRL_Morphine", 1}, {"FRL_epinephrine", 2}

class B_CUP_US_Default { // -- Classname used within the roles. Needs to be unique!
	displayName    = "$str_a3_cfgvehicles_b_soldier_f0"; // -- Name shown in llist
	abilities[] = {}; // -- Which 'abilities' a role has. Options are "RP" (Can place rally points), "Medic" (Can heal more and faster), "Engineer" (Nothing for now, but in future things like putting breaching charges)
	icon    = "frl_kits\ui\icons\rifleman_small_88.paa"; // -- Which icon is shown in the list + on nametags (When you aim at someone)
	marker  = "IconMan"; // -- The icon that's shown on the map and in squad HUD. Current options are "IconMan", "IconManAT", "IconManEngineer", "IconManExplosive" (Grenadier), "IconManLeader", "IconManMedic", "IconManMG", "IconManOfficer", "IconManRecon", "IconManVirtual"
	class Clothing {
		uniform  = "U_B_CombatUniform_mcam"; // -- Uniform to wear
		headgear = "H_HelmetB"; // -- Helmet
		goggles  = ""; // -- Glasses
		vest     = "V_PlateCarrier2_rgr"; // -- Vest
	};

	class Variants {
		class Variant1 { // -- Preset class (Can be whatever)
			displayName = "Standard"; // -- Name of preset (If only 1 preset is present it won't be shown)
			class Pistol {};
			class Primary {
				weapon      = "arifle_MX_F";
				optics      = "optic_Holosight"; // -- Scope
				rail        = ""; // -- Laser sight/flashlight
				muzzle      = ""; // -- Silencers
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_mag_Tracer", 9}}; // -- Magazines that belong to the primary weapon. Shows these items at the weapon instead of in general item list
			};

			class Secondary {}; // -- Launcher
			class Backpack {
				backpack    = "B_Kitbag_mcamo";
				content[] = {{"30Rnd_65x39_caseless_mag_Tracer", 12}}; // -- Stuff in the backpack ["Classname", "Quantity"]. This gets added to backpack only!
			};

			items[] 	= {{"HandGrenade", 2}, {"SmokeShell", 2}}; // -- Items shown in the inspect list. Recommended for any items that actually depend on the role
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL}; // -- Items NOT shown in the inspect list. Use for common things. COMMON_ITEMS gets replaced by whatever it is set to at the top of the file.
		};

		class Variant2 : Variant1 { // -- Second preset. You can have unlimited presets. Only the classes inside this will change. Everything else is the same as the first preset
			displayName = "AR Support"; // -- Second preset name

			class Backpack { // -- Change the backpack type for this class
				backpack    = "B_Bergen_mcamo";
				content[] = {{"100Rnd_65x39_caseless_mag_Tracer", 10}};
			};
		};
	};
};

class B_CUP_US_SL: B_CUP_US_Default { // -- Inheritance! Everything not mentioned here will be the same as B_CUP_US_Default
	displayName = "Squad Leader";
	abilities[] = {"RP"}; // -- Can place rallypoints
	icon    = "frl_kits\ui\icons\sqleader_small_88.paa";
	marker = "IconManOfficer";

	class Variants : Variants { // -- Inherit Variants from B_CUP_US_Default
		class Variant1 : Variant1 { // -- Inherit Variant1 from B_CUP_US_Default
			displayName = "Regular"; // -- Only a single variant so won't get shown
			class Primary : Primary
			{
				weapon      = "arifle_MX_F";
				muzzle      = "muzzle_snds_H";
				rail        = "";
				optics      = "optic_Hamr";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_mag", 2}, {"30Rnd_65x39_caseless_mag_Tracer", 3}}; // -- Tracer mags and non-tracer
			};
			class Backpack {}; // -- Remove the backpack that B_CUP_US_Default has by defult
			class Pistol { // -- Add a pistol
				weapon      = "hgun_Pistol_
				heavy_01_F";
				muzzle      = "muzzle_snds_acp";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"11Rnd_45ACP_Mag", 3}};
			};

			items[] = {{"itemGPS", 1}, {"HandGrenade", 2}, {"SmokeShell", 2}};
		};


		// -- !!! Note how variant2 is not inherited here. In that case it B_CUP_US_SL will only have a single variant.
	};
};

class B_CUP_US_Rifleman: B_CUP_US_Default {
	displayName = "Rifleman";
};

class B_CUP_US_MG: B_CUP_US_Rifleman {
	displayName = "Machinegunner";
	icon = "frl_kits\ui\icons\autorifleman_small_88.paa";
	marker = "IconManMG";

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Light Support";
			class Primary : Primary {
				weapon      = "arifle_MX_SW_F";
				muzzle      = "muzzle_snds_H";
				rail        = "acc_pointer_IR";
				optics      = "optic_tws_mg";
				bipod       = "bipod_01_F_snd";
				magazines[] = {{"100Rnd_65x39_caseless_mag_Tracer", 6}};
			};
			class Backpack {}; // -- Backpack is removed
		};
		class Variant2 : Variant1 { // -- Variant 2 is inherited here because we want a second type
			displayName = "MMG";
			class Primary : Primary {
				weapon      = "MMG_02_camo_F";
				muzzle      = "muzzle_snds_338_sand";
				rail        = "";
				optics      = "optic_Holosight";
				bipod       = "bipod_01_F_mtp";
				magazines[] = {{"130Rnd_338_Mag", 2}};
			};
		};
	};
};
class B_CUP_US_Medic: B_CUP_US_Rifleman {
	displayName = "Medic";
	abilities[] = {"Medic"};
	icon    = "frl_kits\ui\icons\medic_small_88.paa";
	marker = "IconManMedic";
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary {
				weapon      = "arifle_MXC_F";
			};
			class Secondary {};
			class Backpack {
				backpack  = "B_AssaultPack_mcamo";
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_Morphine", 5}, {"FRL_epinephrine", 10}};
			};

			items[] = {{"HandGrenade", 2}, {"SmokeShell", 5}};
			itemshidden[] = {COMMON_ITEMS};
		};
	};
};

class B_CUP_US_Engineer: B_CUP_US_Rifleman {
	displayName = "Engineer";
	abilities[] = {"Engineer"};
	icon    = "frl_kits\ui\icons\engineer_small_88.paa";
	marker = "IconManEngineer";
};

class B_CUP_US_Grenadier: B_CUP_US_Rifleman {
	displayName = "Grenadier";
	icon = "frl_kits\ui\icons\grenadier_small_88.paa";
	marker = "IconManExplosive";
	class Variants : Variants {
		class Variant1 : Variant1 {
			class Primary : Primary {
				weapon      = "arifle_MX_GL_F";
				magazines[] = {{"30Rnd_65x39_caseless_mag_Tracer", 9}, {"1Rnd_HE_Grenade_shell", 8}, {"1Rnd_Smoke_Grenade_shell", 4}};
			};
			class Secondary {};
			class Backpack {};
			items[] = {};
		};
	};
};

class B_CUP_US_Specops_SL: B_CUP_US_Rifleman {
	displayName = "Team Leader";
	abilities[] = {"RP"};
	icon    = "frl_kits\ui\icons\sqleader_small_88.paa";
	marker = "IconManOfficer";

	class Clothing{
		uniform  = "U_I_G_Story_Protagonist_F";
		headgear = "H_HelmetB_black";
		vest     = "V_PlateCarrier2_blk";
		goggles	 = "G_Bandanna_blk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Regular";
			class Primary : Primary {
				weapon      = "arifle_MX_Black_F";
				muzzle      = "muzzle_snds_H";
				rail        = "acc_flashlight";
				optics      = "optic_Holosight";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_mag", 8}};
			};
			class Backpack {
				backpack = "B_AssaultPack_blk";
			};

			class Pistol {
				weapon      = "hgun_Pistol_heavy_01_F";
				muzzle      = "muzzle_snds_acp";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"11Rnd_45ACP_Mag", 3}};
			};

			items[] = {{"itemGPS", 1}, {"HandGrenade", 2}, {"SmokeShell", 2}};
		};
	};
};

class B_CUP_US_Specops_Marksman: B_CUP_US_Specops_SL {
	displayName = "Marksman";
	abilities[] = {};
	icon    = "frl_kits\ui\icons\sniper_small_88.paa";
	marker = "IconManRecon";
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Regular";
			class Primary : Primary {
				weapon      = "srifle_DMR_03_khaki_F";
				muzzle      = "muzzle_snds_B";
				rail        = "";
				optics      = "optic_Arco";
				bipod       = "bipod_01_F_blk";
				magazines[] = {{"20Rnd_762x51_Mag", 8}};
			};
			class Backpack {};
		};
	};
};


class B_CUP_US_Specops_Specialist: B_CUP_US_Specops_Marksman {
	displayName = "Explosives Specialist";
	abilities[] = {"Engineer"};
	icon    = "frl_kits\ui\icons\engineer_small_88.paa";
	marker = "IconManAT";
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Regular";
			class Primary : Primary {
				weapon      = "arifle_MX_Black_F";
				muzzle      = "muzzle_snds_H";
				rail        = "";
				optics      = "optic_Holosight";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_mag", 8}};
			};
			class Backpack {
				backpack = "B_AssaultPack_blk";
				content[] = {{"DemoCharge_Remote_Mag", 3}, {"APERSMine_Range_Mag", 2}};
			};
			class Secondary {};

		};
	};
};
